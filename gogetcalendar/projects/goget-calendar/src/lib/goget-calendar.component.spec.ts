import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GogetCalendarComponent } from './goget-calendar.component';

describe('GogetCalendarComponent', () => {
  let component: GogetCalendarComponent;
  let fixture: ComponentFixture<GogetCalendarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GogetCalendarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GogetCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
