/**
 * @fileoverview added by tsickle
 * Generated from: lib/goget-calendar.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { GogetCalendarComponent } from './goget-calendar.component';
import { CalendarComponent } from './calendar/calendar.component';
export class GogetCalendarModule {
}
GogetCalendarModule.decorators = [
    { type: NgModule, args: [{
                declarations: [GogetCalendarComponent, CalendarComponent],
                imports: [],
                exports: [GogetCalendarComponent]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ29nZXQtY2FsZW5kYXIubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZ29nZXQtY2FsZW5kYXIvIiwic291cmNlcyI6WyJsaWIvZ29nZXQtY2FsZW5kYXIubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUNwRSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQVFsRSxNQUFNLE9BQU8sbUJBQW1COzs7WUFOL0IsUUFBUSxTQUFDO2dCQUNSLFlBQVksRUFBRSxDQUFDLHNCQUFzQixFQUFFLGlCQUFpQixDQUFDO2dCQUN6RCxPQUFPLEVBQUUsRUFDUjtnQkFDRCxPQUFPLEVBQUUsQ0FBQyxzQkFBc0IsQ0FBQzthQUNsQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEdvZ2V0Q2FsZW5kYXJDb21wb25lbnQgfSBmcm9tICcuL2dvZ2V0LWNhbGVuZGFyLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IENhbGVuZGFyQ29tcG9uZW50IH0gZnJvbSAnLi9jYWxlbmRhci9jYWxlbmRhci5jb21wb25lbnQnO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICBkZWNsYXJhdGlvbnM6IFtHb2dldENhbGVuZGFyQ29tcG9uZW50LCBDYWxlbmRhckNvbXBvbmVudF0sXHJcbiAgaW1wb3J0czogW1xyXG4gIF0sXHJcbiAgZXhwb3J0czogW0dvZ2V0Q2FsZW5kYXJDb21wb25lbnRdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBHb2dldENhbGVuZGFyTW9kdWxlIHsgfVxyXG4iXX0=