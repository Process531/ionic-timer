import { NgModule } from '@angular/core';
import { GogetCalendarComponent } from './goget-calendar.component';
import { CalendarComponent } from './calendar/calendar.component';

@NgModule({
  declarations: [GogetCalendarComponent, CalendarComponent],
  imports: [
  ],
  exports: [GogetCalendarComponent]
})
export class GogetCalendarModule { }
