import { TestBed } from '@angular/core/testing';

import { GogetCalendarService } from './goget-calendar.service';

describe('GogetCalendarService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GogetCalendarService = TestBed.get(GogetCalendarService);
    expect(service).toBeTruthy();
  });
});
