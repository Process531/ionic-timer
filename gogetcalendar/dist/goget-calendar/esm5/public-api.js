/**
 * @fileoverview added by tsickle
 * Generated from: public-api.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*
 * Public API Surface of goget-calendar
 */
export { GogetCalendarService } from './lib/goget-calendar.service';
export { GogetCalendarComponent } from './lib/goget-calendar.component';
export { GogetCalendarModule } from './lib/goget-calendar.module';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHVibGljLWFwaS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2dvZ2V0LWNhbGVuZGFyLyIsInNvdXJjZXMiOlsicHVibGljLWFwaS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUlBLHFDQUFjLDhCQUE4QixDQUFDO0FBQzdDLHVDQUFjLGdDQUFnQyxDQUFDO0FBQy9DLG9DQUFjLDZCQUE2QixDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuICogUHVibGljIEFQSSBTdXJmYWNlIG9mIGdvZ2V0LWNhbGVuZGFyXHJcbiAqL1xyXG5cclxuZXhwb3J0ICogZnJvbSAnLi9saWIvZ29nZXQtY2FsZW5kYXIuc2VydmljZSc7XHJcbmV4cG9ydCAqIGZyb20gJy4vbGliL2dvZ2V0LWNhbGVuZGFyLmNvbXBvbmVudCc7XHJcbmV4cG9ydCAqIGZyb20gJy4vbGliL2dvZ2V0LWNhbGVuZGFyLm1vZHVsZSc7XHJcbiJdfQ==