import { Injectable, Component, NgModule, defineInjectable } from '@angular/core';

/**
 * @fileoverview added by tsickle
 * Generated from: lib/goget-calendar.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class GogetCalendarService {
    constructor() { }
}
GogetCalendarService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
GogetCalendarService.ctorParameters = () => [];
/** @nocollapse */ GogetCalendarService.ngInjectableDef = defineInjectable({ factory: function GogetCalendarService_Factory() { return new GogetCalendarService(); }, token: GogetCalendarService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * Generated from: lib/goget-calendar.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class GogetCalendarComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
GogetCalendarComponent.decorators = [
    { type: Component, args: [{
                selector: 'goget-goget-calendar',
                template: `
    <p>
      goget-calendar works!
    </p>
  `
            }] }
];
/** @nocollapse */
GogetCalendarComponent.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * Generated from: lib/calendar/calendar.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CalendarComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
CalendarComponent.decorators = [
    { type: Component, args: [{
                selector: 'goget-calendar',
                template: "<p>\r\n  calendar works!\r\n</p>\r\n",
                styles: [""]
            }] }
];
/** @nocollapse */
CalendarComponent.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * Generated from: lib/goget-calendar.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class GogetCalendarModule {
}
GogetCalendarModule.decorators = [
    { type: NgModule, args: [{
                declarations: [GogetCalendarComponent, CalendarComponent],
                imports: [],
                exports: [GogetCalendarComponent]
            },] }
];

/**
 * @fileoverview added by tsickle
 * Generated from: public-api.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * Generated from: goget-calendar.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { GogetCalendarService, GogetCalendarComponent, GogetCalendarModule, CalendarComponent as ɵa };

//# sourceMappingURL=goget-calendar.js.map