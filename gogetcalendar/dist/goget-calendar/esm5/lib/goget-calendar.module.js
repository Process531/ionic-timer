/**
 * @fileoverview added by tsickle
 * Generated from: lib/goget-calendar.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { GogetCalendarComponent } from './goget-calendar.component';
import { CalendarComponent } from './calendar/calendar.component';
var GogetCalendarModule = /** @class */ (function () {
    function GogetCalendarModule() {
    }
    GogetCalendarModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [GogetCalendarComponent, CalendarComponent],
                    imports: [],
                    exports: [GogetCalendarComponent]
                },] }
    ];
    return GogetCalendarModule;
}());
export { GogetCalendarModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ29nZXQtY2FsZW5kYXIubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZ29nZXQtY2FsZW5kYXIvIiwic291cmNlcyI6WyJsaWIvZ29nZXQtY2FsZW5kYXIubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUNwRSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUVsRTtJQUFBO0lBTW1DLENBQUM7O2dCQU5uQyxRQUFRLFNBQUM7b0JBQ1IsWUFBWSxFQUFFLENBQUMsc0JBQXNCLEVBQUUsaUJBQWlCLENBQUM7b0JBQ3pELE9BQU8sRUFBRSxFQUNSO29CQUNELE9BQU8sRUFBRSxDQUFDLHNCQUFzQixDQUFDO2lCQUNsQzs7SUFDa0MsMEJBQUM7Q0FBQSxBQU5wQyxJQU1vQztTQUF2QixtQkFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBHb2dldENhbGVuZGFyQ29tcG9uZW50IH0gZnJvbSAnLi9nb2dldC1jYWxlbmRhci5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBDYWxlbmRhckNvbXBvbmVudCB9IGZyb20gJy4vY2FsZW5kYXIvY2FsZW5kYXIuY29tcG9uZW50JztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgZGVjbGFyYXRpb25zOiBbR29nZXRDYWxlbmRhckNvbXBvbmVudCwgQ2FsZW5kYXJDb21wb25lbnRdLFxyXG4gIGltcG9ydHM6IFtcclxuICBdLFxyXG4gIGV4cG9ydHM6IFtHb2dldENhbGVuZGFyQ29tcG9uZW50XVxyXG59KVxyXG5leHBvcnQgY2xhc3MgR29nZXRDYWxlbmRhck1vZHVsZSB7IH1cclxuIl19